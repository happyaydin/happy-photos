//
//  IENavigation.swift
//
//
//  Created by Hasan Asan on 08/03/16.
//  Copyright © 2017 Happy Digital. All rights reserved.
//

import Foundation
import UIKit

class Navigation {
  
  class func showAlert(_ title: String, message: String) -> Void {
    let alert = UIAlertController(title: title,
                                  message: message,
                                  preferredStyle: .alert)
    let okAction = UIAlertAction(
      title: "Tamam",
      style: .default) { (_) in
        alert.dismiss(animated: true,
                      completion: nil)
    }
    alert.addAction(okAction)
    self.topViewCtlr().present(alert,
                               animated: true,
                               completion: nil)
  }
  
  
  class func shareSheet(_ text: String?,image: UIImage?,url:URL?){
    var sharingItems = [AnyObject]()
    
    if text != nil {
      sharingItems.insert(text! as AnyObject, at: 0)
    }
    
    if image != nil {
      sharingItems.insert(image!, at: 0)
    }
    
    if url != nil {
      sharingItems.insert(url! as AnyObject, at: 0)
    }
    
    let shareCtlr = UIActivityViewController(activityItems: sharingItems,
                                             applicationActivities: nil)
    
    self.topViewCtlr().present(shareCtlr,
                               animated: true,
                               completion: nil)
    
  }
  
  class func topViewCtlr() -> UIViewController{
    let keyWindow: UIWindow = UIApplication.shared.keyWindow!
    var ctlr: UIViewController = keyWindow.rootViewController!
    
    if (ctlr.presentedViewController != nil){
      ctlr = ctlr.presentedViewController!
    }
    return ctlr
  }

  class func setRootViewCtlr(_ viewctlr: UIViewController) {
    let window: UIWindow = (UIApplication.shared.delegate as! AppDelegate).window!
    window.rootViewController = viewctlr
  }

  class func hideKeyboard() {
    UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder),
                                    to:   nil,
                                    from: nil,
                                    for:  nil)
  }

  class func tabCount() -> Int {
    let window = (UIApplication.shared.delegate as! AppDelegate).window!
    let tabController = window.rootViewController as? UITabBarController
    if tabController == nil {
      return 0
    }

    return tabController!.viewControllers?.count ?? 0
  }

  class func setTabbarIndex(_ index:    TabbarIndex,
                            _ animated: Bool = false) -> UIViewController? {

    let window = (UIApplication.shared.delegate as! AppDelegate).window!
    let tabController = window.rootViewController as? UITabBarController
    if tabController == nil {
      return nil
    }

    let rootOfTab = tabController?.selectedViewController as? UINavigationController
    if rootOfTab?.isKind(of: UINavigationController.self) ?? false {
      rootOfTab?.popToRootViewController(animated: animated)
    }

    tabController?.selectedIndex = index.rawValue

    return tabController?.selectedViewController
  }
}
