//
//  BaseView.swift
//  HappyPhotos
//
//  Created by Recep Aslantas on 12/14/17.
//  Copyright © 2017 Happy Digital. All rights reserved.
//

import UIKit

class BaseView: UIView {
  @IBInspectable var cornerRadius: CGFloat = 0 {
    didSet {
      layer.cornerRadius = cornerRadius
      layer.masksToBounds = cornerRadius > 0
    }
  }

  @IBInspectable var borderWidth: CGFloat = 0 {
    didSet {
      layer.borderWidth = borderWidth
    }
  }

  @IBInspectable var borderColor: UIColor? {
    didSet {
      layer.borderColor = borderColor?.cgColor
    }
  }

  @IBInspectable var shadowColor: UIColor? {
    didSet {
      layer.shadowColor = shadowColor?.cgColor
    }
  }


  @IBInspectable var shadowOpacity: Float = 0 {
    didSet {
      layer.shadowOpacity = shadowOpacity
    }
  }

  @IBInspectable var shadowRadius: CGFloat = 0 {
    didSet {
      layer.shadowRadius = shadowRadius
    }
  }

  @IBInspectable var shadowOffsetX: CGFloat = 0 {
    didSet {
      layer.shadowOffset = CGSize(width:  shadowOffsetX,
                                  height: layer.shadowOffset.height)
    }
  }

  @IBInspectable var shadowOffsetY: CGFloat = 0 {
    didSet {
      layer.shadowOffset = CGSize(width:  layer.shadowOffset.width,
                                  height: shadowOffsetY)
    }
  }
}
