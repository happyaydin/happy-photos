//
//  ColorPickerView.swift
//  HappyPhotos
//
//  Created by Recep Aslantas on 2/12/18.
//  Copyright © 2018 Happy Digital. All rights reserved.
//

import UIKit

class ColorPickerView: HSBColorPicker, HSBColorPickerDelegate {
  var selectedColorView: UIView!

  weak var pickerdelegate: ColorPickerViewDelegate?

  override init(frame: CGRect) {
    super.init(frame: frame)
    self.commonInit()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    self.commonInit()
  }

  func commonInit() {
    self.delegate = self
    self.elementSize = 20

    self.selectedColorView = UIView(frame: CGRect(x: 0,
                                                  y: 0,
                                                  width:  self.elementSize,
                                                  height: self.elementSize))
    self.selectedColorView.layer.borderColor    = #colorLiteral(red: 1, green: 0.8430734201, blue: 0.1315542702, alpha: 1)
    self.selectedColorView.layer.borderWidth    = 2.0
    self.selectedColorView.layer.cornerRadius   = 2.0

    self.selectedColorView.layer.shadowColor    = UIColor(white: 0.5, alpha: 0.5).cgColor
    self.selectedColorView.layer.shadowOpacity  = 0.8
    self.selectedColorView.layer.shadowOffset   = CGSize(width: 0.5, height: 0.5)
    self.selectedColorView.layer.shadowRadius   = 1.5

    self.addSubview(self.selectedColorView)
  }

  func HSBColorColorPickerTouched(sender: HSBColorPicker,
                                  color:  UIColor,
                                  point:  CGPoint,
                                  state:  UIGestureRecognizerState) {
    let halfElemSize = self.elementSize * 0.5
    let x = point.x - point.x.truncatingRemainder(dividingBy: self.elementSize) + halfElemSize
    let y = point.y - point.y.truncatingRemainder(dividingBy: self.elementSize) + halfElemSize

    let center = CGPoint(x: x, y: y)
    self.selectedColorView.center = center
    self.pickerdelegate?.colorPickerView(didSelect: self,
                                         color:     color,
                                         point:     center)
  }
}

internal protocol ColorPickerViewDelegate : NSObjectProtocol {
  func colorPickerView(didSelect picker: ColorPickerView,
                       color: UIColor,
                       point: CGPoint)
}
