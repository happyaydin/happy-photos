//
//  DashedView.swift
//  HappyPhotos
//
//  Created by Recep Aslantas on 12/18/17.
//  Copyright © 2017 Happy Digital. All rights reserved.
//

import UIKit

class DashedView: UIView {
  var startPoint = CGPoint.zero
  var endPoint   = CGPoint.zero

  override init(frame: CGRect) {
    super.init(frame: frame)
    self.commonInit()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    self.commonInit()
  }

  func commonInit() {
    self.backgroundColor = UIColor.clear
    self.isUserInteractionEnabled = false
  }

  override func draw(_ rect: CGRect) {
    let path = UIBezierPath()

    path.move(to: self.startPoint)
    path.addLine(to: self.endPoint)
    
    let dashes: [ CGFloat ] = [ 2.0, 4.0 ]
    path.setLineDash(dashes, count: dashes.count, phase: 0.0)
    path.lineWidth = 1.0
    path.lineCapStyle = .round
    #colorLiteral(red: 1, green: 0.8430734201, blue: 0.1315542702, alpha: 1).set()
    path.stroke()
  }
}
