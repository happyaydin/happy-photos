//
//  ColorKeyboard.swift
//  HappyPhotos
//
//  Created by Recep Aslantas on 12/25/17.
//  Copyright © 2017 Happy Digital. All rights reserved.
//

import UIKit

class ColorKeyboard: UIInputViewController, HSBColorPickerDelegate {
  @IBOutlet weak var selectedColorView: UIView!

  weak var picker: HSBColorPicker!
  weak var delegate: ColorKeyboardDelegate?

  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.translatesAutoresizingMaskIntoConstraints = false

    self.picker = (self.view as! HSBColorPicker)
    self.picker.delegate = self

    self.selectedColorView.frame.size = CGSize(width:  picker.elementSize,
                                               height: picker.elementSize)
  }

  func HSBColorColorPickerTouched(sender: HSBColorPicker,
                                  color:  UIColor,
                                  point:  CGPoint,
                                  state:  UIGestureRecognizerState) {
    let x = point.x + point.x.truncatingRemainder(dividingBy: self.picker.elementSize)
    let y = point.y + point.y.truncatingRemainder(dividingBy: self.picker.elementSize)

    self.selectedColorView.center = CGPoint(x: x, y: y)
    self.delegate?.colorKeyboardDidSelectColor(sender: self, color: color)
  }
}

internal protocol ColorKeyboardDelegate : NSObjectProtocol {
  func colorKeyboardDidSelectColor(sender: ColorKeyboard,
                                   color:  UIColor)
}
