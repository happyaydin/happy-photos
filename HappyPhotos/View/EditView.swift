//
//  EditView.swift
//  HappyPhotos
//
//  Created by Recep Aslantas on 12/14/17.
//  Copyright © 2017 Happy Digital. All rights reserved.
//

import UIKit

protocol EditViewDelegate: class {
  func didMove(_ editView: EditView, point: CGPoint)
  func touchBegan(_ editView: EditView)
  func touchEnded(_ editView: EditView)
  func editView(tapped editView: EditView)
  func editView(dblTapped editView: EditView)
}

class EditView: BaseView, UIGestureRecognizerDelegate {
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var textView:  UITextView!

  static var activeView: EditView? = nil
  static var activeDate: Date?     = nil

  weak var delegate: EditViewDelegate?

  var contentView:      UIView!
  var accessoryView:    UIView!
  var keyboardCtlr:     ColorKeyboard!

  var prevTransform:    CGAffineTransform!
  var currScale:        CGFloat! = 1
  var currRotation:     CGFloat! = 0

  var tapGest:   UITapGestureRecognizer!
  var dblGest:   UITapGestureRecognizer!
  var panGest:   UIPanGestureRecognizer!
  var pinchGest: UIPinchGestureRecognizer!
  var rotGest:   UIRotationGestureRecognizer!

  var showColorPicker              = false
  var lostFocusFired               = false
  var colorPoint: CGPoint          = CGPoint(x: 10, y: 10)

  var font: UIFont! {
    willSet {
      self.textView.font = newValue;
    }
  }

  // MARK: ctors / initializers
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override init(frame: CGRect) {
    let extend = CGFloat(20.0)
    super.init(frame: CGRect(origin: CGPoint(x: frame.origin.x - extend * 0.5,
                                             y: frame.origin.y - extend * 0.5),
                             size:   CGSize(width:  frame.width  + extend,
                                            height: frame.height + extend)))

    if (self.subviews.count == 0) {
      self.backgroundColor = .clear
      let nib        = UINib(nibName: "EditView", bundle: Bundle.main)
      let nibObjects = nib.instantiate(withOwner: self, options: nil)

      self.contentView   = nibObjects[0] as! UIView
      self.accessoryView = nibObjects[1] as! UIView

      self.contentView.frame = self.bounds
      self.addSubview(self.contentView)

      self.keyboardCtlr = ColorKeyboard(nibName: "ColorKeyboard", bundle: Bundle.main)
      self.keyboardCtlr.delegate = self

      self.tapGest    = UITapGestureRecognizer(target: self,      action: #selector(self.tapGesture))
      self.dblGest    = UITapGestureRecognizer(target: self,      action: #selector(self.dblGesture))
      self.panGest    = UIPanGestureRecognizer(target: self,      action: #selector(self.panGesture))
      self.pinchGest  = UIPinchGestureRecognizer(target: self,    action: #selector(self.pinchGesture))
      self.rotGest    = UIRotationGestureRecognizer(target: self, action: #selector(self.rotGesture))

      self.dblGest.numberOfTapsRequired = 2
      self.tapGest.numberOfTapsRequired = 1
      self.tapGest.require(toFail: self.dblGest)

      self.panGest.delaysTouchesBegan = false

      self.addGestureRecognizer(self.tapGest)
      self.addGestureRecognizer(self.dblGest)
      self.addGestureRecognizer(self.panGest)
      self.addGestureRecognizer(self.pinchGest)
      self.addGestureRecognizer(self.rotGest)

      self.tapGest.delegate   = self
      self.dblGest.delegate   = self
      self.panGest.delegate   = self
      self.pinchGest.delegate = self
      self.rotGest.delegate   = self

      self.tapGest.cancelsTouchesInView   = false
      self.dblGest.cancelsTouchesInView   = false
      self.panGest.cancelsTouchesInView   = false
      self.pinchGest.cancelsTouchesInView = false
      self.rotGest.cancelsTouchesInView   = false

      self.imageView.layer.shadowColor    = UIColor(white: 0.5, alpha: 1).cgColor
      self.imageView.layer.shadowOpacity  = 0.3
      self.imageView.layer.shadowOffset   = .zero
      self.imageView.layer.shadowRadius   = 10

      NotificationCenter.default.addObserver(self,
                                             selector: #selector(keyboardWillShow(notification:)),
                                             name:     NSNotification.Name.UIKeyboardWillShow,
                                             object:   nil)

      NotificationCenter.default.addObserver(self,
                                             selector: #selector(handleHide(_ :)),
                                             name:     NTF_EDIT_HIDE_ALL,
                                             object:   nil)

      NotificationCenter.default.addObserver(self,
                                             selector: #selector(handleLostFocus(_ :)),
                                             name:     NTF_EDIT_LOSTFOCUS,
                                             object:   nil)

      NotificationCenter.default.addObserver(self,
                                             selector: #selector(handleFocus(_ :)),
                                             name:     NTF_EDIT_FOCUS,
                                             object:   nil)
    }
  }

  convenience init(_ frame: CGRect, _ image: UIImage) {
    self.init(frame: frame)
    self.imageView.image = image
  }

  override var inputAccessoryView: UIView? {
    return self.accessoryView
  }

  override var inputViewController: UIInputViewController? {
    if self.showColorPicker {
      return self.keyboardCtlr
    }

    return nil
  }

  // MARK: Utils
  func beginTyping() {
    self.textView.centerVertically()
    self.textView.isUserInteractionEnabled = true
    self.textView.becomeFirstResponder()
  }

  func applyColor(_ color: UIColor) {
    self.imageView.tintColor = color
  }

  // MARK: Notifications
  @objc func keyboardWillShow(notification: NSNotification){
    var info = notification.userInfo!
    let f = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue

    self.keyboardCtlr.view.frame.size = CGSize(width:  f!.width,
                                               height: f!.height - self.accessoryView.frame.height)
  }

  @objc func handleHide(_ notification: NSNotification){
    self.textView.resignFirstResponder()
  }

  @objc func handleLostFocus(_ notification: NSNotification){
    if notification.object as? EditView == self {
      UIView.animate(withDuration: 0.2) {
        self.alpha = 1.0
      }
      return
    }

    UIView.animate(withDuration: 0.2) {
      self.alpha = 0.4
    }
  }

  @objc func handleFocus(_ notification: NSNotification){
    UIView.animate(withDuration: 0.2) {
      self.alpha = 1.0
    }

    EditView.activeView = nil
  }

  // MARK: Actions
  @IBAction func removeAction(_ sender: Any) {
    var trans = CGAffineTransform(translationX: 0, y: self.frame.height * 1.5)
    trans     = trans.scaledBy(x: 0.001, y: 0.001)

    UIView.animate(withDuration: 0.3, animations: {
      self.transform = trans
    }) { _ in
      EditView.activeView = nil
      NotificationCenter.default.post(name: NTF_EDIT_FOCUS, object: nil)
      self.removeFromSuperview()
    }
  }

  @IBAction func toggleKeyboardAction(_ sender: UIButton) {
    sender.isSelected    = !sender.isSelected
    self.showColorPicker = !self.showColorPicker
    self.textView.reloadInputViews()
  }
  
  @IBAction func finishTypingAction(_ sender: Any) {
    self.textView.resignFirstResponder()

    if self.textView.text.trimmingCharacters(in: [" "]).count == 0 {
      self.removeAction(sender)
    }
  }

  // MARK: Gest
  // MARK: - Gesture Recognizer Delegate

  func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer,
                         shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
    return true
  }

  // MARK: - Gesture Recognizers

  @objc func tapGesture(sender: UITapGestureRecognizer) {
    if self.textView.isUserInteractionEnabled {
      self.beginTyping()
    }

    self.delegate?.editView(tapped: self)

    /* cunku touchesEnded cagirilmiyor */
    DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
      self.delegate?.touchEnded(self)

      EditView.activeDate = Date()
      if !self.lostFocusFired {
        self.lostFocus()
      }
    }
  }

  @objc func dblGesture(sender: UITapGestureRecognizer) {
    if self.textView.isUserInteractionEnabled {
      self.beginTyping()
    }

    self.delegate?.editView(dblTapped: self)

    /* cunku touchesEnded cagirilmiyor */
    DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
      self.delegate?.touchEnded(self)

      EditView.activeDate = Date()
      if !self.lostFocusFired {
        self.lostFocus()
      }
    }
  }

  @objc func panGesture(sender: UIPanGestureRecognizer) {
    let translation = sender.translation(in: self.superview)
    let point       = sender.location(in: self.superview)

    self.center = CGPoint(x: translation.x + self.center.x,
                          y: translation.y + self.center.y)
    sender.setTranslation(.zero, in: self.superview)
    self.delegate?.didMove(self, point: point)
  }

  @objc func pinchGesture(sender: UIPinchGestureRecognizer) {
    let scale = sender.scale
    if sender.state == .began {
      if self.prevTransform == nil {
        self.prevTransform = self.transform
      }
    } else if sender.state == .changed {
      self.currScale = scale
      self.applyTrans()
    } else if sender.state == .ended {
      self.currScale     = 1
      self.prevTransform = nil
    }
  }

  @objc func rotGesture(sender: UIRotationGestureRecognizer) {
    let rotation = sender.rotation
    if sender.state == .began {
      if self.prevTransform == nil {
        self.prevTransform = self.transform
      }
    } else if sender.state == .changed {
      self.currRotation = rotation
      self.applyTrans()
    } else if sender.state == .ended {
      self.currRotation  = 0
      self.prevTransform = nil
    }
  }

  func applyTrans()  {
    let scale = max(self.currScale, 0.0001)
    self.transform = self.prevTransform
                         .scaledBy(x: scale, y: scale)
                         .rotated(by: self.currRotation)
  }

  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    super.touchesBegan(touches, with: event)
    self.delegate?.touchBegan(self)

    EditView.activeView = self
    EditView.activeDate = Date(timeIntervalSinceNow: 2)
    NotificationCenter.default.post(name: NTF_EDIT_LOSTFOCUS, object: self)
  }

  override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    super.touchesEnded(touches, with: event)
    EditView.activeDate = Date(timeIntervalSinceNow: 2)
  }

  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    super.touchesEnded(touches, with: event)
    self.delegate?.touchEnded(self)

    EditView.activeDate = Date()
    if !self.lostFocusFired {
      self.lostFocus()
    }
  }

  func lostFocus() {
    self.lostFocusFired = true
    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
      if EditView.activeView == self {
        if EditView.activeDate?.compare(Date()) != .orderedDescending {
          NotificationCenter.default.post(name: NTF_EDIT_FOCUS, object: nil)
        } else {
          self.lostFocus()
        }
      }
      self.lostFocusFired = false
    }
  }

  override func layoutSubviews() {
    super.layoutSubviews()
    self.textView.centerVertically()
  }

  deinit {
    NotificationCenter.default.removeObserver(self)
  }
}

extension EditView: UITextViewDelegate {
  func textView(_ textView: UITextView,
                shouldChangeTextIn range: NSRange,
                replacementText text: String) -> Bool {
    textView.centerVertically()
    return true
  }
}

extension EditView: ColorKeyboardDelegate {
  func colorKeyboardDidSelectColor(sender: ColorKeyboard, color: UIColor) {
    self.textView.textColor = color
  }
}
