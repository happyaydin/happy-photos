//
//  Enums.swift
//  HappyPhotos
//
//  Created by Recep Aslantas on 2/14/18.
//  Copyright © 2018 Happy Digital. All rights reserved.
//

import Foundation

enum TabbarIndex : Int {
  case Home          = 0
  case Other         = 1
}
