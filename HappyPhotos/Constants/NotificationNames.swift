//
//  NotificationNames.swift
//  HappyPhotos
//
//  Created by Recep Aslantas on 12/27/17.
//  Copyright © 2017 Happy Digital. All rights reserved.
//

import Foundation

// Notifications
let NTF_EDIT_HIDE_ALL  = NSNotification.Name(rawValue: "HP:EDIT:HIDE-ALL")
let NTF_EDIT_LOSTFOCUS = NSNotification.Name(rawValue: "HP:EDIT:LOST-FOCUS")
let NTF_EDIT_FOCUS     = NSNotification.Name(rawValue: "HP:EDIT:FOCUS")
let NTF_TOGGLE_SHAD    = NSNotification.Name(rawValue: "HP:TOGGLE:SHAD")
