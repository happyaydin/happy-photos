//
//  StickerPack.swift
//  HappyPhotos
//
//  Created by Recep Aslantas on 12/13/17.
//  Copyright © 2017 Happy Digital. All rights reserved.
//

import UIKit

class StickerPack: NSObject {
  var name:   String!
  var icon:   UIImage!

  var stickers = [Sticker]()

  init(_ name: String, _ icon: UIImage!) {
    self.name = name
    self.icon = icon.withRenderingMode(.alwaysTemplate)
  }
}
