//
//  Sticker.swift
//  HappyPhotos
//
//  Created by Recep Aslantas on 12/13/17.
//  Copyright © 2017 Happy Digital. All rights reserved.
//

import UIKit

class Sticker: NSObject {
  var packId: Int!
  var thumb:  String!
  var img:    String!

  init(_ thumb: String, _ img: String) {
    self.thumb  = thumb
    self.img    = img
  }
}
