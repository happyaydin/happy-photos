//
//  View.swift
//  HappyPhotos
//
//  Created by Recep Aslantas on 12/26/17.
//  Copyright © 2017 Happy Digital. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
  func takeScreenshot() -> UIImage {
    UIGraphicsBeginImageContextWithOptions(frame.size,
                                           isOpaque,
                                           UIScreen.main.scale)
    self.layer.render(in: UIGraphicsGetCurrentContext()!)
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()

    return image!
  }

  func takeScreenshot(_ frm: CGRect) -> UIImage {
    UIGraphicsBeginImageContextWithOptions(frm.size,
                                           isOpaque,
                                           UIScreen.main.scale);

    let newOrigin = CGPoint(x: -frm.origin.x, y: -frm.origin.y)
    let newBounds = CGRect(origin: newOrigin, size: self.bounds.size)
    self.drawHierarchy(in: newBounds, afterScreenUpdates: true)
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()

    return image!
  }
}

