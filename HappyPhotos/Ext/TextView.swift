//
//  TextView.swift
//  HappyPhotos
//
//  Created by Recep Aslantas on 12/21/17.
//  Copyright © 2017 Happy Digital. All rights reserved.
//

import UIKit

extension UITextView {
  func centerVertically() {
    self.superview?.layoutIfNeeded()

    let fittingSize       = CGSize(width: bounds.width, height: CGFloat.greatestFiniteMagnitude)
    let size              = sizeThatFits(fittingSize)
    let topOffset         = (bounds.size.height - size.height * zoomScale) * 0.5
    let positiveTopOffset = max(1, topOffset)
    contentOffset.y       = -positiveTopOffset
  }
}
