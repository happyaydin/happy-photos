//
//  MoreViewController.swift
//  HappyPhotos
//
//  Created by Recep Aslantas on 2/14/18.
//  Copyright © 2018 Happy Digital. All rights reserved.
//

import UIKit
import MessageUI
import OneSignal

enum MoreMenuType {
  case review, feedback, other
}

class MoreMenu {
  var title:    String!
  var segue:    String!
  var deeplink: String!
  var type:     MoreMenuType!

  init(_ segue: String = "",
       _ link:  String = "",
       _ title: String,
       _ type:  MoreMenuType = .other) {
    self.title    = title
    self.segue    = segue
    self.deeplink = link
    self.type     = type
  }
}

class HappyApp {
  var name:     String!
  var desc:     String!
  var iconName: String!
  var link:     String!

  init(_ name: String, _ icon: String, _ link: String) {
    self.name     = NSLocalizedString("App:\(name):Title", comment: "")
    self.desc     = NSLocalizedString("App:\(name):Desc",  comment: "")
    self.iconName = icon
    self.link     = link
  }
}

class AppCell: UITableViewCell {
  @IBOutlet weak var iconImageView: UIImageView!
  @IBOutlet weak var nameLabel:     UILabel!
  @IBOutlet weak var descLabel:     UILabel!

  func prepare(_ item: HappyApp) {
    self.iconImageView.image  = UIImage(named: item.iconName)
    self.nameLabel.text       = item.name
    self.descLabel.text       = item.desc
  }
}

class MoreMenuTableViewCell: UITableViewCell {
  @IBOutlet weak var cellTitle: UILabel!

  func prepare(_ title : String){
    self.cellTitle.text = NSLocalizedString(title, comment: "")
  }
}

class MoreViewController: UIViewController {
  @IBOutlet weak var menuList:      UITableView!
  @IBOutlet weak var appsTableView: UITableView!
  @IBOutlet weak var scrollView:    UIScrollView!

  var menus: [MoreMenu] = [
//  MoreMenu("sg_products", "products", "OurProducts"),
//    MoreMenu("sg_apps",     "apps",     "OurApps"),
    MoreMenu("",            "review",   "ReviewUs", .review),
    MoreMenu("",            "feedback", "Feedback", .feedback)
  ]

  var apps: [HappyApp] = [
    HappyApp("HappyMom",  "app_icon_happymom",  "https://itunes.apple.com/us/app/mutlu-anne-hamilelik-takibi/id1141379201?ls=1&mt=8"),
    HappyApp("Pedometer", "app_icon_pedometer", "https://itunes.apple.com/us/app/adım-sayar/id736071203?ls=1&mt=8"),
    HappyApp("Peride",     "app_icon_peride",    "https://itunes.apple.com/us/app/periyot-günlüğü-peride/id577097723?ls=1&mt=8"),
    ]

  let screenName = "Diğer Menü"
  var menuToGo: String?

  override func viewDidLoad() {
    super.viewDidLoad()
    let imageView = UIImageView(image: UIImage(named: "menu_logo_beyaz"))
    self.navigationController?.navigationBar.topItem?.titleView = imageView
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    self.navigationController?.setNavigationBarHidden(false, animated: true)
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    if self.menuToGo != nil {
      self.navigateToMenu(self.menuToGo!)
      self.menuToGo = nil
    }

    self.scrollView.contentSize = CGSize(width:  self.scrollView.frame.width,
                                         height: self.menuList.contentSize.height
                                               + self.appsTableView.contentSize.height)
  }

  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
  }

  func navigateToMenu(_ manuLink: String) {
    let link = manuLink.lowercased()
    if self.viewIfLoaded?.window == nil {
      self.menuToGo = link
      return
    }

    for (index, menu) in self.menus.enumerated() {
      if menu.deeplink == link || "\(index + 1)" == link {
        let indexPath = IndexPath(row: index, section: 0)
        self.menuList.selectRow(at: indexPath,
                                animated: false,
                                scrollPosition: .top)
        self.tableView(self.menuList, didSelectRowAt: indexPath)
        break
      }
    }
  }
}

extension MoreViewController: UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  func tableView(_ tableView: UITableView,
                 numberOfRowsInSection section: Int) -> Int {
    if tableView == self.appsTableView {
      return self.apps.count
    }

    return self.menus.count
  }

  func tableView(_ tableView: UITableView,
                 cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if tableView == self.appsTableView {
      let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! AppCell
      let item = self.apps[indexPath.item]
      cell.prepare(item)
      return cell
    }
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MoreMenuTableViewCell
    cell.prepare(self.menus[indexPath.row].title)
    cell.backgroundColor = .clear
    return cell
  }
}

extension MoreViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView,
                 didSelectRowAt indexPath: IndexPath) {
    if tableView == self.appsTableView {
      let item = self.apps[indexPath.item]
      let url  = URL(string: item.link.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)

      UIApplication.shared.open(url!, options: [:]) { _ in  }
      return
    }

    let menu = self.menus[indexPath.row]

    if menu.type == .review {
      let appLink = "https://itunes.apple.com/us/app/happy-photos/id1317783824?ls=1&mt=8"

      let url = URL(string: appLink)
      if UIApplication.shared.canOpenURL(url!) {
        let alert = UIAlertController(title: NSLocalizedString("WillExitTitle", comment: ""),
                                      message: NSLocalizedString("WillExitBody", comment: ""),
                                      preferredStyle: .alert)

        let confirm = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default, handler: { (_) in
          UIApplication.shared.open(url!, options: [:]) { _ in  }
        })
        let decline = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default, handler: nil)

        alert.addAction(decline)
        alert.addAction(confirm)

        self.present(alert, animated: true, completion: nil)
      }
    } else if menu.type == .feedback {
      if MFMailComposeViewController.canSendMail() {
        let mc: MFMailComposeViewController = MFMailComposeViewController()
        let emailTitle  = NSLocalizedString("FeedbackEmailTitle", comment: "")
        let toRecipents = ["happy@happydigital.com.tr"]
        let dictionary  = Bundle.main.infoDictionary!
        let version     = dictionary["CFBundleShortVersionString"] as! String
        let build       = dictionary["CFBundleVersion"] as! String

        let playerId    = UserDefaults.standard.string(forKey: "HP:OS:PlayerId") ?? "-"

        let bodyText    = String.init(format: NSLocalizedString("FeedbackEmailBody",
                                                                comment: ""),
                                      version,
                                      build,
                                      UIDevice.current.systemVersion,
                                      UIDevice.current.modelName,
                                      playerId)
//        let attributesDictionary =
//          [
//            NSAttributedStringKey.foregroundColor : color
//        ]
//        mc.navigationBar.titleTextAttributes = attributesDictionary
//        mc.navigationBar.tintColor = color
        mc.mailComposeDelegate = self
        mc.setSubject(emailTitle)
        mc.setToRecipients(toRecipents)
        mc.setMessageBody(bodyText, isHTML: true)
        self.present(mc, animated: true, completion: nil)
      } else {
        Navigation.showAlert(NSLocalizedString("AlertErrorTitle", comment: ""),
                             message: NSLocalizedString("EmailConfBody", comment: ""))
      }
    } else {
      tableView.deselectRow(at: indexPath,
                            animated: true)
      self.performSegue(withIdentifier: menu.segue, sender: self)
    }
  }
}

extension MoreViewController : MFMailComposeViewControllerDelegate {
  func mailComposeController(_ controller: MFMailComposeViewController,
                             didFinishWith result: MFMailComposeResult, error: Error?) {
    switch result {
    case .cancelled:
      print("Mail cancelled")
    case .saved:
      print("Mail saved")
    case .sent:
      print("Mail sent")
    case .failed:
      print("Mail sent failure: \(String(describing: error?.localizedDescription))")
    }
    self.dismiss(animated: true, completion: nil)
  }
}
