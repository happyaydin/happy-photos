//
//  HomeViewController.swift
//  HappyPhotos
//
//  Created by Recep Aslantas on 2/21/18.
//  Copyright © 2018 Happy Digital. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
  @IBOutlet weak var menuView:        UIView!
  @IBOutlet weak var sliderImageView: UIImageView!
  @IBOutlet var menuButons: [UIButton]!

  var sliderTimer:   Timer!
  var sliderCount:   Int = 5
  var _sliderCurrent: Int = 1
  var sliderCurrent: Int {
    get {
      return _sliderCurrent
    }

    set {
      if newValue > self.sliderCount {
        _sliderCurrent = 1
      } else {
        _sliderCurrent = newValue
      }

      UserDefaults.standard.set(_sliderCurrent, forKey: "HP:SLIDER:CURR")
      UserDefaults.standard.synchronize()
    }
  }

  var sliderPhotoNamePrefix = "slider_photo_"

  override func viewDidLoad() {
    super.viewDidLoad()

    let userInterface = UIDevice.current.userInterfaceIdiom
    if(userInterface == .pad){
      self.sliderPhotoNamePrefix += "ipad_"
    }

    self.menuView.layer.cornerRadius  = 8

    self.menuView.layer.shadowColor   = UIColor.black.cgColor
    self.menuView.layer.shadowOpacity = 0.7
    self.menuView.layer.shadowOffset  = CGSize(width: 0, height: 4)
    self.menuView.layer.shadowRadius  = 4

    self.navigationController?.setNavigationBarHidden(true, animated: false)

    for b in self.menuButons {
      b.centerTextWithImage(offset: 32)
    }

    self.sliderCurrent = (UserDefaults.standard.object(forKey: "HP:SLIDER:CURR") ?? 1) as! Int
    self.slide()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    self.navigationController?.setNavigationBarHidden(true, animated: true)
    self.fireTimers()
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    self.invalidateTimers()
  }

  func slide() {
    let imageName      = "\(self.sliderPhotoNamePrefix)\(self.sliderCurrent)"
    let image          = UIImage(named: imageName)

    let fadeAnim       = CABasicAnimation(keyPath: "contents");
    fadeAnim.fromValue = self.sliderImageView.image!
    fadeAnim.toValue   = image
    fadeAnim.duration  = 0.7

    self.sliderImageView.layer.add(fadeAnim, forKey: "contents")
    self.sliderImageView.image = image
  }

  func fireTimers() {
    self.invalidateTimers()
    self.sliderTimer = Timer.scheduledTimer(timeInterval: 3,
                                            target:       self,
                                            selector:     #selector(self.slidertimerTick),
                                            userInfo:     nil,
                                            repeats:      true)
  }

  func invalidateTimers() {
    if self.sliderTimer != nil {
      self.sliderTimer.invalidate()
    }
  }

  @objc func slidertimerTick(_ timer: Timer) {
    self.sliderCurrent += 1
    self.slide()
  }

  @IBAction func newAction(_ sender: Any) {
    let bartint             = self.navigationController?.navigationBar.barTintColor
    let tint                = self.navigationController?.navigationBar.tintColor
    let titleTextAttributes = self.navigationController?.navigationBar.titleTextAttributes

    let actionSheet = UIAlertController.init(title: nil,
                                             message: nil,
                                             preferredStyle: .actionSheet)
    if UIImagePickerController.isSourceTypeAvailable(.camera){
      let cameraAction = UIAlertAction.init(title: NSLocalizedString("Camera", comment: ""),
                                            style: .default) { (_) in

        let imagePicker           = UIImagePickerController()
        imagePicker.delegate      = self
        imagePicker.sourceType    = .camera
        imagePicker.allowsEditing = false

        imagePicker.navigationBar.barTintColor        = bartint
        imagePicker.navigationBar.tintColor           = tint
        imagePicker.navigationBar.titleTextAttributes = titleTextAttributes
        imagePicker.navigationBar.isTranslucent       = false
        self.present(imagePicker,
                     animated: true,
                     completion: nil)
      }

      actionSheet.addAction(cameraAction)
    }

    let galleryAction = UIAlertAction.init(title: NSLocalizedString("Gallery", comment: ""),
                                           style: .default) { (_) in
      if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
        let imagePicker = UIImagePickerController()
        imagePicker.delegate      = self
        imagePicker.sourceType    = .photoLibrary
        imagePicker.allowsEditing = false

        imagePicker.navigationBar.barTintColor        = bartint
        imagePicker.navigationBar.tintColor           = tint
        imagePicker.navigationBar.titleTextAttributes = titleTextAttributes
        imagePicker.navigationBar.isTranslucent       = false
        self.present(imagePicker, animated: true, completion: nil)
      }
    }

    let cancelAction = UIAlertAction.init(title: NSLocalizedString("Cancel", comment: ""),
                                          style: .cancel) { (_) in
      actionSheet.dismiss(animated: true, completion: nil)
    }

    actionSheet.addAction(galleryAction)
    actionSheet.addAction(cancelAction)

    if let presenter = actionSheet.popoverPresentationController {
      presenter.sourceView = self.menuButons[0]
      presenter.sourceRect = self.menuButons[0].bounds
    }

    self.present(actionSheet, animated: true, completion: nil)
  }

  @IBAction func galleryAction(_ sender: Any) {
    self.performSegue(withIdentifier: "sg_gallery", sender: self)
  }

  @IBAction func othersAction(_ sender: Any) {
    self.performSegue(withIdentifier: "sg_others", sender: self)
  }

  // MARK:
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "sg_edit" {
      let dest = segue.destination as! MainViewController
      dest.setImage(sender! as! UIImage)
    }
  }
}

extension HomeViewController: UINavigationControllerDelegate { }
extension HomeViewController: UIImagePickerControllerDelegate {
  func imagePickerController(_ picker: UIImagePickerController,
                             didFinishPickingMediaWithInfo info: [String : Any]) {
    if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
      self.editPhoto(image: image)
    }

    picker.dismiss(animated: true, completion: nil);
  }

  func imagePickerController(_ picker: UIImagePickerController,
                             didFinishPickingImage image: UIImage,
                             editingInfo: [String : AnyObject]?) {
    picker.dismiss(animated: true) {
      self.editPhoto(image: image)
    }
  }

  func editPhoto(image: UIImage) {
    self.performSegue(withIdentifier: "sg_edit", sender: image)
  }
}
