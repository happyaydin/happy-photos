//
//  PregnancyViewController.swift
//  happy-mom
//
//  Created by Hasan Asan on 05/06/2017.
//  Copyright © 2017 Happy Digital. All rights reserved.
//

import UIKit
import PureLayout
import WebKit
import NVActivityIndicatorView

class ProductsViewController: UIViewController, WKUIDelegate, WKNavigationDelegate {
  @IBOutlet var contentView:   UIView!
  @IBOutlet var reloadBarItem: UIBarButtonItem!

  var indicator:    NVActivityIndicatorView!
  var webView:      WKWebView!
  var popupWebView: WKWebView!

  let blogHostBase    = "photos.mutluannelerimiz.com"
  let blogHost        = "photos.mutluannelerimiz.com"
  let blogURLString   = "https://photos.mutluannelerimiz.com"

  let screenName      = "Products"

  let refreshControl = UIRefreshControl()

  func commonInit() {
    let prefs = WKPreferences()
    prefs.javaScriptEnabled = true
    prefs.javaScriptCanOpenWindowsAutomatically = true

    let config = WKWebViewConfiguration()
    config.preferences = prefs

    webView = WKWebView(frame: .zero, configuration: config)
    webView.uiDelegate         = self
    webView.navigationDelegate = self
    self.contentView.addSubview(webView)
    webView.autoPinEdgesToSuperviewEdges()
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    self.commonInit()

    self.indicator = NVActivityIndicatorView(frame: CGRect(x: 0,
                                                           y: 0,
                                                           width: 50,
                                                           height: 50),
                                             type: .lineScale,
                                             color: #colorLiteral(red: 1, green: 0.8430734201, blue: 0.1315542702, alpha: 1),
                                             padding: 0)
    self.view.addSubview(self.indicator)
    self.indicator.autoCenterInSuperview()

    self.refreshControl.tintColor = #colorLiteral(red: 1, green: 0.8430734201, blue: 0.1315542702, alpha: 1)
    self.webView.scrollView.addSubview(self.refreshControl)
    self.refreshControl.addTarget(self,
                                  action: #selector(refreshAction(_:)),
                                  for: .valueChanged)

    self.indicator.startAnimating()
    self.webView.load(URLRequest(url: URL(string: self.blogURLString)!))
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
  }

  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
  }

  func createAndLoadInterstitial() {
//    if self.isViewLoaded && self.view.window != nil {
//      self.interstitial          = GADInterstitial(adUnitID: UnitID.pregnancyInterstitial.desc)
//      let request                = GADRequest()
//      self.interstitial.delegate = self
//      self.interstitial.load(request)
//    }
//
//    self.scheduleInterstitial()
  }

  func scheduleInterstitial() {
    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(180)) { [weak self] in
      self?.createAndLoadInterstitial()
    }
  }

  // MARK: Actions

  @IBAction func refreshAction(_ sender: Any) {
    self.webView.reload()
  }

  @IBAction func shareAction(_ sender: Any) {
    if self.webView.url == nil {
      return
    }

    var sharingItems = [AnyObject]()
    sharingItems.insert(self.webView.url! as AnyObject, at: 0)
    let shareCtlr = UIActivityViewController(activityItems: sharingItems,
                                             applicationActivities: nil)

    if UIDevice.current.userInterfaceIdiom == .pad {
      shareCtlr.popoverPresentationController?.sourceView = self.view
    }

    self.present(shareCtlr,
                 animated: true,
                 completion: nil)

  }

  // MARK: WKWebView
  func webView(_ webView: WKWebView,
               createWebViewWith configuration: WKWebViewConfiguration,
               for navigationAction: WKNavigationAction,
               windowFeatures: WKWindowFeatures) -> WKWebView? {
    popupWebView = WKWebView(frame: CGRect.zero,
                             configuration: configuration)
    self.webView.addSubview(popupWebView!)
    return popupWebView
  }

  func webView(_ webView: WKWebView,
               runJavaScriptAlertPanelWithMessage message: String,
               initiatedByFrame frame: WKFrameInfo,
               completionHandler: @escaping () -> Void) {
    let alertController = UIAlertController(title: nil,
                                            message: message,
                                            preferredStyle: .alert)
    alertController.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""),
                                            style: .default,
                                            handler: { (action) in
      completionHandler()
    }))

    present(alertController, animated: true, completion: nil)
  }

  func webView(_ webView: WKWebView,
               runJavaScriptConfirmPanelWithMessage message: String,
               initiatedByFrame frame: WKFrameInfo,
               completionHandler: @escaping (Bool) -> Void) {

    let alertController = UIAlertController(title: nil,
                                            message: message,
                                            preferredStyle: .alert)

    alertController.addAction(UIAlertAction(title: NSLocalizedString("Yes", comment: ""),
                                            style: .default,
                                            handler: { (action) in
      completionHandler(true)
    }))

    alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""),
                                            style: .default,
                                            handler: { (action) in
      completionHandler(false)
    }))

    present(alertController, animated: true, completion: nil)
  }

  func webView(_ webView: WKWebView,
               didFinish navigation: WKNavigation!) {
    if (popupWebView != nil) {
      popupWebView?.removeFromSuperview()
      popupWebView = nil
    }

    if self.indicator.isAnimating {
      self.indicator.stopAnimating()
    }

    self.refreshControl.endRefreshing()
  }

  func webView(_ webView: WKWebView,
               didFail navigation: WKNavigation!,
               withError error: Error) {
    if self.indicator.isAnimating {
      self.indicator.stopAnimating()
    }

    self.refreshControl.endRefreshing()
  }

  func webView(_ webView: WKWebView,
               decidePolicyFor navigationAction: WKNavigationAction,
               decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
    if navigationAction.request.url?.host == nil {
      decisionHandler(.cancel)
      return
    }

    if (navigationAction.request.url?.host!.hasSuffix(self.blogHost))!
      || (navigationAction.request.url?.host!.hasSuffix(self.blogHostBase))! {
      if navigationAction.navigationType == .linkActivated {
        self.performSegue(withIdentifier: "sg_detail",
                          sender: navigationAction.request)
        decisionHandler(.cancel)
        return
      } else {
        self.indicator.startAnimating()
        decisionHandler(.allow)
      }

      return;
    }

    if navigationAction.navigationType == .linkActivated {
      UIApplication.shared.open(navigationAction.request.url!, options: [:]) { _ in  }
      decisionHandler(.cancel)
      return
    }

    self.indicator.startAnimating()
    decisionHandler(.allow)
  }

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "sg_detail" {
      let ctlr      = segue.destination as! ProductsDetailViewController
      ctlr.request  = sender as? URLRequest
      ctlr.hostName = self.blogHost
    }
  }

  deinit {
    NotificationCenter.default.removeObserver(self)
  }
}

//extension ProductsViewController: GADInterstitialDelegate {
//  func interstitialDidReceiveAd(_ ad: GADInterstitial) {
//    if self.isViewLoaded && self.view.window != nil {
//      self.interstitial.present(fromRootViewController: self)
//    }
//  }
//
//  func interstitial(_ ad: GADInterstitial,
//                    didFailToReceiveAdWithError error: GADRequestError) {
//    print(error)
//  }
//}

