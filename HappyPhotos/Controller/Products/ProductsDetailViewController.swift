//
//  BlogDetailViewController.swift
//  happy-mom
//
//  Created by Recep Aslantas on 8/1/17.
//  Copyright © 2017 Happy Digital. All rights reserved.
//

import UIKit
import WebKit
import KVNProgress
import NVActivityIndicatorView
import PureLayout

class ProductsDetailViewController: UIViewController, WKUIDelegate, WKNavigationDelegate {
  @IBOutlet var indicator: NVActivityIndicatorView!

  var webView:      WKWebView!
  var popupWebView: WKWebView!
  var hostName:     String!

  private var _request: URLRequest?
  var request: URLRequest? {
    get {
      return _request
    }

    set {
      _request        = newValue
      if newValue != nil {
        self.screenName = newValue!.url!.absoluteString
      }

      if !self.isViewLoaded {
        return
      }

      if _request != nil {
        self.webView.load(_request!)
      } else {
        self.webView.loadHTMLString("", baseURL: nil)
      }
    }
  }

  var screenName = "Products - Detail"

  let refreshControl = UIRefreshControl()
  
  override func loadView() {
    let prefs = WKPreferences()
    prefs.javaScriptEnabled = true
    prefs.javaScriptCanOpenWindowsAutomatically = true

    let config = WKWebViewConfiguration()
    config.preferences = prefs

    webView = WKWebView(frame: .zero, configuration: config)
    webView.uiDelegate = self
    webView.navigationDelegate = self
    view = webView

    self.refreshControl.tintColor = #colorLiteral(red: 1, green: 0.8430734201, blue: 0.1315542702, alpha: 1)
    webView.scrollView.addSubview(self.refreshControl)
    self.refreshControl.addTarget(self,
                                  action: #selector(refreshAction(_:)),
                                  for: .valueChanged)

    self.indicator = NVActivityIndicatorView(frame: CGRect(x: 0,
                                                           y: 0,
                                                           width: 50,
                                                           height: 50),
                                             type: .lineScale,
                                             color: #colorLiteral(red: 1, green: 0.8430734201, blue: 0.1315542702, alpha: 1),
                                             padding: 0)
    self.view.addSubview(self.indicator)
    self.indicator.autoCenterInSuperview()
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    if self.request != nil {
      self.webView.load(self.request!)
    }
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
    self.navigationController?.navigationBar.isHidden = false
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.scheduleInterstitial()
  }

  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
  }

  @IBAction func refreshAction(_ sender: Any) {
    self.webView.reload()
  }
  
  @IBAction func shareAction(_ sender: Any) {
    if self.request == nil {
      return
    }

    var sharingItems = [AnyObject]()
    let url = self.request?.url?.absoluteString

    if url != nil {
      sharingItems.insert(url! as AnyObject, at: 0)
      let shareCtlr = UIActivityViewController(activityItems: sharingItems,
                                               applicationActivities: nil)

      if UIDevice.current.userInterfaceIdiom == .pad {
        shareCtlr.popoverPresentationController?.sourceView = self.view
      }

      self.present(shareCtlr,
                   animated: true,
                   completion: nil)
    }
  }

  func createAndLoadInterstitial() {
//    if self.isViewLoaded && self.view.window != nil {
//      self.interstitial          = GADInterstitial(adUnitID: UnitID.pregnancyInterstitial.desc)
//      let request                = GADRequest()
//      self.interstitial.delegate = self
//      self.interstitial.load(request)
//    }
//
//    self.scheduleInterstitial()
  }

  func scheduleInterstitial() {
    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(180)) { [weak self] in
      self?.createAndLoadInterstitial()
    }
  }

  // MARK: WKWebView
  func webView(_ webView: WKWebView,
               createWebViewWith configuration: WKWebViewConfiguration,
               for navigationAction: WKNavigationAction,
               windowFeatures: WKWindowFeatures) -> WKWebView? {
    popupWebView = WKWebView(frame: self.webView.frame,
                             configuration: configuration)
    self.webView.addSubview(popupWebView!)
    return popupWebView
  }

  func webView(_ webView: WKWebView,
               runJavaScriptAlertPanelWithMessage message: String,
               initiatedByFrame frame: WKFrameInfo,
               completionHandler: @escaping () -> Void) {
    let alertController = UIAlertController(title: nil,
                                            message: message,
                                            preferredStyle: .alert)
    alertController.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""),
                                            style: .default,
                                            handler: { (action) in
                                              completionHandler()
    }))

    present(alertController, animated: true, completion: nil)
  }

  func webView(_ webView: WKWebView,
               runJavaScriptConfirmPanelWithMessage message: String,
               initiatedByFrame frame: WKFrameInfo,
               completionHandler: @escaping (Bool) -> Void) {

    let alertController = UIAlertController(title: nil,
                                            message: message,
                                            preferredStyle: .alert)

    alertController.addAction(UIAlertAction(title: NSLocalizedString("Yes", comment: ""),
                                            style: .default,
                                            handler: { (action) in
                                              completionHandler(true)
    }))

    alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""),
                                            style: .default,
                                            handler: { (action) in
                                              completionHandler(false)
    }))

    present(alertController, animated: true, completion: nil)
  }

  func webView(_ webView: WKWebView,
               didFinish navigation: WKNavigation!) {
    if (popupWebView != nil) {
      popupWebView?.removeFromSuperview()
      popupWebView = nil
    }

    if self.isViewLoaded {
      self.indicator.stopAnimating()
    }

    self.title = self.webView.title
    self.refreshControl.endRefreshing()
  }

  func webView(_ webView: WKWebView,
               didFail navigation: WKNavigation!,
               withError error: Error) {
    if self.isViewLoaded {
      self.indicator.stopAnimating()
    }

    self.refreshControl.endRefreshing()
  }

  func webView(_ webView: WKWebView,
               decidePolicyFor navigationAction: WKNavigationAction,
               decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
    if navigationAction.request.url?.host == nil {
      decisionHandler(.cancel)
      return
    }

    if (navigationAction.request.url?.host?.hasSuffix(self.hostName))! {
      self.indicator.startAnimating()
      decisionHandler(.allow)
      return
    }

    if navigationAction.navigationType == .linkActivated {
      decisionHandler(.cancel)
      UIApplication.shared.open(navigationAction.request.url!, options: [:]) { _ in  }
      return
    }

    decisionHandler(.allow)
  }

  deinit {
  }
}

//extension ProductsDetailViewController: GADInterstitialDelegate {
//  func interstitialDidReceiveAd(_ ad: GADInterstitial) {
//    if self.isViewLoaded && self.view.window != nil {
//      self.interstitial.present(fromRootViewController: self)
//    }
//  }
//
//  func interstitial(_ ad: GADInterstitial,
//                    didFailToReceiveAdWithError error: GADRequestError) {
//    print(error)
//  }
//}

