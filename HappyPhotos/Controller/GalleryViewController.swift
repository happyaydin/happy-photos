//
//  ArchiveViewController.swift
//  HappyPhotos
//
//  Created by Recep Aslantas on 12/22/17.
//  Copyright © 2017 Happy Digital. All rights reserved.
//

import UIKit
import Photos

class PhotoCell: UICollectionViewCell {
  @IBOutlet weak var imageView: UIImageView!

  func prepare(_ image: UIImage?) {
    self.imageView.image = image
  }
}

class GalleryViewController: UIViewController,
                             UIImagePickerControllerDelegate,
                             UINavigationControllerDelegate,
                             UICollectionViewDelegate,
                             UICollectionViewDataSource,
                             UICollectionViewDelegateFlowLayout,
                             PHPhotoLibraryChangeObserver {

  @IBOutlet weak var collecitonView: UICollectionView!
  @IBOutlet weak var appSettingsButton: UIButton!
  @IBOutlet weak var appSettingsContainer: UIView!
  @IBOutlet weak var emptyDescView: UIView!
  
  var assetCollection:          PHAssetCollection!
  var photosAsset:              PHFetchResult<AnyObject>!
  var assetThumbnailSize:       CGSize!
  var cellSize:                 CGSize!

  // TODO: global bi yere tasi, Constants, Shared gibi
  var albumName = "Happy Photos";

  let imagePicker = UIImagePickerController()

  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.checkLibraryPermission()
    
    self.prepareCellSize()

    self.imagePicker.delegate = self
    
    PHPhotoLibrary.shared().register(self)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.navigationController?.setNavigationBarHidden(false, animated: true)
    self.reload()
  }

  func prepareAlbums() {
    // Happy photos
    self.assetCollection = PHPhotoLibrary.shared().findAlbum(albumName: self.albumName)
    if self.assetCollection == nil {
      PHPhotoLibrary.shared().createAlbum(albumName: self.albumName, completion: { collection in
        self.assetCollection = collection
      })
    }
  }

  func reload() {
    if PHPhotoLibrary.authorizationStatus() != .authorized {
      self.photosAsset = nil
      self.collecitonView.performBatchUpdates({
        self.collecitonView.reloadSections([0])
      })
      return
    }

    let fetchOpt = PHFetchOptions()
    let sorcDesc = [NSSortDescriptor(key: "creationDate",     ascending: false),
                    NSSortDescriptor(key: "modificationDate", ascending: false)]
    fetchOpt.sortDescriptors = sorcDesc
    fetchOpt.fetchLimit      = 50

    if self.assetCollection != nil {
      self.photosAsset = (PHAsset.fetchAssets(in: self.assetCollection,
                                              options: fetchOpt) as AnyObject!) as! PHFetchResult<AnyObject>!
    }


    self.collecitonView.performBatchUpdates({
      self.collecitonView.reloadSections([0])
    })
  }

  func prepareCellSize() {
    let cellCountInRow = 3.0
    let cellSpace      = 2.0
    let cellWidth = (Double(self.view.frame.width) - cellSpace * (cellCountInRow + 1)) / cellCountInRow
    self.cellSize = CGSize(width: cellWidth, height: cellWidth)
  }
  
  func checkLibraryPermission() {
    if PHPhotoLibrary.authorizationStatus() != .authorized {
      self.appSettingsButton.layer.cornerRadius = 4
      self.appSettingsButton.layer.borderWidth = 1
      self.appSettingsButton.layer.borderColor = UIColor(red:   154.0/255.0,
                                                         green: 154.0/255.0,
                                                         blue:  154.0/255.0,
                                                         alpha: 1).cgColor
      self.appSettingsButton.layer.masksToBounds = true

      PHPhotoLibrary.requestAuthorization({ (status) in
        if status == .authorized {
          DispatchQueue.main.async {
            self.prepareAlbums()
            self.reload()
          }
        }

        DispatchQueue.main.async {
          self.appSettingsContainer.isHidden = status == .authorized
//          self.emptyDescLabel.isHidden = status == .authorized
        }
      })

      return
    }

     self.prepareAlbums()
  }
  
  func goToSettings() {
    if  UIApplication.shared.canOpenURL(URL(string:UIApplicationOpenSettingsURLString)!) {
      UIApplication.shared.open(URL(string:UIApplicationOpenSettingsURLString)!,
                                options: [:],
                                completionHandler: nil)
    }
  }
  
  @IBAction func goToSettingsAction(_ sender: Any) {
    self.goToSettings()
  }
  
  // MARK: UIImagePicker

  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    picker.dismiss(animated: true, completion: nil)
  }

  func imagePickerController(_ picker: UIImagePickerController,
                             didFinishPickingMediaWithInfo info: [String : Any]) {
    let image = info[UIImagePickerControllerOriginalImage] as! UIImage
    self.performSegue(withIdentifier: "sg_edit", sender: image)
    picker.dismiss(animated: true, completion: nil)
  }
  
  // MARK: UICollectionView
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }

  func collectionView(_ collectionView: UICollectionView,
                      numberOfItemsInSection section: Int) -> Int {
    if self.photosAsset == nil {
      return 0
    }
    
    self.emptyDescView.isHidden = self.photosAsset.count != 0

    return self.photosAsset.count;
  }

  func collectionView(_ collectionView: UICollectionView,
                      cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell",
                                                  for: indexPath) as! PhotoCell
    let asset = self.photosAsset[indexPath.item] as! PHAsset
    PHImageManager.default().requestImage(for:          asset,
                                          targetSize:   self.cellSize,
                                          contentMode: .aspectFill,
                                          options:      nil,
                                          resultHandler: { (result, info) in
      if let image = result {
        cell.prepare(image)
      }
    })

    cell.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
    return cell
  }

  func collectionView(_ collectionView: UICollectionView,
                      willDisplay cell: UICollectionViewCell,
                      forItemAt indexPath: IndexPath) {
    UIView.animate(withDuration: 0.3) {
      cell.transform = .identity
    }
  }

  func collectionView(_ collectionView: UICollectionView,
                      didEndDisplaying cell: UICollectionViewCell,
                      forItemAt indexPath: IndexPath) {
    cell.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
  }

  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      sizeForItemAt indexPath: IndexPath) -> CGSize {
    return self.cellSize
  }

  func collectionView(_ collectionView: UICollectionView,
                      didSelectItemAt indexPath: IndexPath) {
    let asset   = self.photosAsset[indexPath.item] as! PHAsset
    let options = PHImageRequestOptions()
    options.isSynchronous = true
    PHImageManager.default().requestImage(for:          asset,
                                          targetSize:   self.view.bounds.size,
                                          contentMode: .aspectFit,
                                          options:      options,
                                          resultHandler: { (result, info) in
      if let image = result {
        self.performSegue(withIdentifier: "sg_edit", sender: image)
      }
    })
  }

  // MARK: Segue
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "sg_edit" {
      let dest = segue.destination as! MainViewController
      dest.setImage(sender! as! UIImage)
    }
  }
  
  // MARK: PHPhotoLibraryChangeObserver
  func photoLibraryDidChange(_ changeInstance: PHChange) {
    DispatchQueue.main.async {
      self.reload()
    }
  }
}
