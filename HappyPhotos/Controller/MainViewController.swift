//
//  MainViewController.swift
//  HappyPhotos
//
//  Created by Recep Aslantas on 11/28/17.
//  Copyright © 2017 Happy Digital. All rights reserved.
//

import UIKit
import CoreImage
import AVFoundation
import Photos
import KVNProgress
import Firebase

enum EditSectionType {
  case filters, stickerPacks, texts
}

class SelectFilterCell: UICollectionViewCell {
  @IBOutlet weak var imageView: UIImageView!
}

class StickerPackCell: UICollectionViewCell {
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
}

class StickerCell: UICollectionViewCell {
  @IBOutlet weak var imageView: UIImageView!
}

class TextCell: UICollectionViewCell {
  @IBOutlet weak var label: UILabel!
}

// Daha guzel bir isim bulunabilir
class CollectionViewsWrapper: UIView {
  @IBOutlet weak var collectionView:    UICollectionView!
  @IBOutlet weak var subCollectionView: UICollectionView!

  override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
    if !self.collectionView.isScrollEnabled {
      let p = self.subCollectionView.convert(point, from: self)
      if self.subCollectionView.bounds.contains(p) {
        return self.subCollectionView.hitTest(p, with: event)
      }
    }

    let p = self.collectionView.convert(point, from: self)
    return self.collectionView.hitTest(p, with: event)
  }
}

class MainViewController: UIViewController {
  @IBOutlet weak var editorView:            UIView!
  @IBOutlet weak var effectsView:           UIView!
  @IBOutlet weak var imageView:             UIImageView!
  @IBOutlet weak var collectionViewWrapper: CollectionViewsWrapper!
  @IBOutlet var buttons:                    [UIControl]!
  @IBOutlet weak var subCollvLeadingConst:  NSLayoutConstraint!
  @IBOutlet weak var deleteButton:          UIButton!

  @IBOutlet weak var colorPickerContainerView: UIView!
  @IBOutlet weak var colorPicker: ColorPickerView!
  @IBOutlet weak var hideColorPickerView: UIButton!

  var context:          CIContext!
  var filter:           CIFilter!
  var image:            UIImage!
  var rawImageSmall:    CIImage!
  var section:          EditSectionType = .filters
  var selectedPack:     StickerPack?
  var imageFrame:       CGRect! {
    get {
      let img = self.imageView.image
      if img == nil {
        return CGRect.zero
      }

      return self.view.convert(AVMakeRect(aspectRatio: img!.size,
                                          insideRect:  self.imageView.frame),
                               from: self.imageView.superview)
    }
  }

  var stickerPacks = [StickerPack]()
  var selectedItem: EditView?

  var filterNames = [
    "CIPhotoEffectFade",
    "CIPhotoEffectChrome",
    "CIPhotoEffectFade",
    "CIPhotoEffectInstant",
    "CIPhotoEffectNoir",
    "CIPhotoEffectProcess",
    "CIPhotoEffectTonal",
    "CIPhotoEffectTransfer",
    "CISepiaTone"
  ]

  var fontNames = [
    "Yellowtail",
    "Swiss-NormalTr",
    "Raleway-Regular",
    "CarlottaTR",
    "Austin-Italic"
    ]

  // TODO: global bi yere tasi, Constants, Shared gibi
  var albumName = "Happy Photos"

  override func viewDidLoad() {
    super.viewDidLoad()

    self.loadLocalData()
    self.imageView.image = self.image

    self.subCollvLeadingConst.constant  = (self.view.frame.width - 8 * 6) * 0.2
    self.deleteButton.layer.anchorPoint = CGPoint(x: 0.5, y: 1.0)
    self.deleteButton.alpha             = 0.0
    self.colorPicker.pickerdelegate     = self

    self.colorPickerContainerView.layer.shadowColor   = UIColor.black.cgColor
    self.colorPickerContainerView.layer.shadowOpacity = 0.25
    self.colorPickerContainerView.layer.shadowOffset  = CGSize.zero
    self.colorPickerContainerView.layer.shadowRadius  = 10

    self.colorPickerContainerView.layer.anchorPoint = CGPoint(x: 0.5, y: 1.0)
    self.colorPickerContainerView.transform = CGAffineTransform(scaleX: 1, y: 0.0001)
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.navigationController?.setNavigationBarHidden(false, animated: true)
  }

  func setImage(_ image: UIImage) {
    self.image         = image.fixOrientation()
    let imageSmall     = image.resizeImage(targetSize: CGSize(width: 80, height: 80))
    self.rawImageSmall = CIImage(image: imageSmall)
    self.context       = CIContext(options: nil)
  }

  func loadLocalData() {
    do {
      let stickerPackJsonURL  = Bundle.main.url(forResource: "stickersPack",
                                                withExtension: "json")
      let stickerPackJsonData = try Data(contentsOf: stickerPackJsonURL!)
      let stickerPackJson     = try
        JSONSerialization.jsonObject(with:    stickerPackJsonData,
                                     options: .allowFragments) as! [[String: AnyHashable]]

      for sp in stickerPackJson {
        let spack = StickerPack(sp["loc_name"]            as! String,
                                UIImage(named: sp["icon"] as! String))
        let stickerJson = sp["stickers"] as! [[String: AnyHashable]]
        for s in stickerJson {
          let sticker = Sticker(s["thumb"] as! String,
                                s["img"]   as! String)
          spack.stickers.append(sticker)
        }

        self.stickerPacks.append(spack)
      }
    } catch { }
  }

  // MARK: Actions
  @IBAction func cropAction(_ sender: Any) {
    let controller = ResizeViewController()
    controller.delegate = self
    controller.image = self.imageView.image

    let navController = UINavigationController(rootViewController: controller)
    navController.navigationBar.tintColor    = self.navigationController?.navigationBar.tintColor
    navController.navigationBar.barTintColor = self.navigationController?.navigationBar.barTintColor
    self.present(navController, animated: true, completion: nil)
  }

  @IBAction func filtersAction(_ sender: UIButton) {
    self.collectionViewWrapper.subCollectionView.alpha = 0.0

    self.section = .filters
    self.collectionViewWrapper.collectionView.reloadData()

    self.collectionViewWrapper.collectionView.isScrollEnabled    = true
    self.collectionViewWrapper.subCollectionView.isScrollEnabled = false
    self.collectionViewWrapper.subCollectionView.alpha           = 0.0

    UIView.animate(withDuration: 0.3) {
      sender.backgroundColor = UIColor.init(white: 0.15, alpha: 1.0)
      for b in self.buttons {
        if b != sender {
          b.backgroundColor = UIColor.init(white: 0.1, alpha: 1.0)
        }
      }
    }

    Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
      AnalyticsParameterItemID: "id-filter-btn" as NSObject,
      AnalyticsParameterItemName: "Filters Button" as NSObject,
      AnalyticsParameterContentType: "cont" as NSObject
    ])
  }

  @IBAction func stickersAction(_ sender: UIButton) {
    self.collectionViewWrapper.subCollectionView.alpha = 0.0

    self.section = .stickerPacks
    self.collectionViewWrapper.collectionView.reloadData()

    self.collectionViewWrapper.collectionView.isScrollEnabled    = true
    self.collectionViewWrapper.subCollectionView.isScrollEnabled = false
    self.collectionViewWrapper.subCollectionView.alpha           = 0.0

    UIView.animate(withDuration: 0.3) {
      sender.backgroundColor = UIColor.init(white: 0.15, alpha: 1.0)
      for b in self.buttons {
        if b != sender {
          b.backgroundColor = UIColor.init(white: 0.1, alpha: 1.0)
        }
      }
    }

    Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
      AnalyticsParameterItemID: "id-stickers-btn" as NSObject,
      AnalyticsParameterItemName: "Stickers Button" as NSObject,
      AnalyticsParameterContentType: "cont" as NSObject
    ])
  }

  @IBAction func textsAction(_ sender: UIButton) {
    self.collectionViewWrapper.subCollectionView.alpha = 0.0

    self.section = .texts
    self.collectionViewWrapper.collectionView.reloadData()

    self.collectionViewWrapper.collectionView.isScrollEnabled    = true
    self.collectionViewWrapper.subCollectionView.isScrollEnabled = false
    self.collectionViewWrapper.subCollectionView.alpha           = 0.0

    UIView.animate(withDuration: 0.3) {
      sender.backgroundColor = UIColor.init(white: 0.15, alpha: 1.0)
      for b in self.buttons {
        if b != sender {
          b.backgroundColor = UIColor.init(white: 0.1, alpha: 1.0)
        }
      }
    }

    Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
      AnalyticsParameterItemID: "id-texts-btn" as NSObject,
      AnalyticsParameterItemName: "Texts Button" as NSObject,
      AnalyticsParameterContentType: "cont" as NSObject
    ])
  }

  @IBAction func saveAction(_ sender: Any) {
    let imgFrame = AVMakeRect(aspectRatio: self.imageView.image!.size,
                              insideRect:  self.imageView.frame)
    let screenshot = self.editorView.takeScreenshot(imgFrame)

    PHPhotoLibrary.shared().savePhoto(image: screenshot, albumName: self.albumName)
    KVNProgress.showSuccess {}

    Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
      AnalyticsParameterItemID: "id-photo-save" as NSObject,
      AnalyticsParameterItemName: "Photo Save" as NSObject,
      AnalyticsParameterContentType: "cont" as NSObject
    ])
  }

  @IBAction func shareAction(_ sender: UIBarButtonItem) {
    NotificationCenter.default.post(name: NTF_EDIT_HIDE_ALL, object: nil)
    NotificationCenter.default.post(name: NTF_EDIT_FOCUS, object: nil)

    self.hideColorPicAction(sender)

    DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(400)) {
      var sharingItems = [AnyObject]()
      let imgFrame     = AVMakeRect(aspectRatio: self.imageView.image!.size,
                                    insideRect:  self.imageView.frame)

      let screenshot = self.editorView.takeScreenshot(imgFrame)
      sharingItems.insert(screenshot as AnyObject, at: 0)
      let shareCtlr = UIActivityViewController(activityItems: sharingItems,
                                               applicationActivities: nil)

      if UIDevice.current.userInterfaceIdiom == .pad {
        shareCtlr.popoverPresentationController?.barButtonItem = sender
      }

      self.present(shareCtlr, animated: true, completion: nil)
    }

    Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
      AnalyticsParameterItemID: "id-photo-share" as NSObject,
      AnalyticsParameterItemName: "Photo Share" as NSObject,
      AnalyticsParameterContentType: "cont" as NSObject
    ])
  }

  @IBAction func hideColorPicAction(_ sender: Any) {
    if self.colorPickerContainerView.transform.d < 0.1 {
      return
    }

    UIView.animate(withDuration: 0.3, animations: {
      self.colorPickerContainerView.transform = CGAffineTransform(scaleX: 1, y: 1.05)
    }) { _ in
      UIView.animate(withDuration: 0.3, animations: {
        self.colorPickerContainerView.transform = CGAffineTransform(scaleX: 1, y: 0.0001)
      })
    }
  }

  @IBAction func hideAllAction(_ sender: Any) {
    NotificationCenter.default.post(name: NTF_EDIT_HIDE_ALL, object: nil)
  }
}

extension MainViewController: UICollectionViewDelegate,
                              UICollectionViewDataSource,
                              UICollectionViewDelegateFlowLayout {
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }

  func collectionView(_ collectionView: UICollectionView,
                      numberOfItemsInSection section: Int) -> Int {
    if self.rawImageSmall == nil {
      return 0
    }

    if collectionView == self.collectionViewWrapper.subCollectionView {
      return self.selectedPack?.stickers.count ?? 0
    }

    switch self.section {
    case .filters:      return self.filterNames.count
    case .stickerPacks: return self.stickerPacks.count
    case .texts:        return self.fontNames.count
    }
  }

  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      sizeForItemAt indexPath: IndexPath) -> CGSize {
    if collectionView == self.collectionViewWrapper.subCollectionView {
      return CGSize(width: 50, height: 50)
    }

    switch self.section {
    case .filters:
      return CGSize(width: 50, height: 50)
    case .stickerPacks:
      return CGSize(width: (self.view.frame.width - 8 * 6) * 0.2, height: 50)
    case .texts:
      return CGSize(width: 80, height: 50)
    }
  }

  func collectionView(_ collectionView: UICollectionView,
                      cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    if collectionView == self.collectionViewWrapper.subCollectionView {
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sticker",
                                                    for: indexPath) as! StickerCell
      let sticker          = self.selectedPack!.stickers[indexPath.row]
      cell.imageView.image = UIImage(named: sticker.thumb)
      cell.transform       = CGAffineTransform(scaleX: 0, y: 0)
      return cell
    }

    switch self.section {
    case .filters:
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell",
                                                    for: indexPath) as! SelectFilterCell
      let filter = CIFilter(name: "\(self.filterNames[indexPath.row])")

      filter!.setDefaults()
      filter!.setValue(self.rawImageSmall, forKey: kCIInputImageKey)

      let outputImage = filter!.outputImage
      let cgImage     = context.createCGImage(outputImage!, from: outputImage!.extent)

      cell.imageView.image = UIImage(cgImage: cgImage!)
      cell.transform       = CGAffineTransform.identity
      return cell
    case .stickerPacks:
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "spack",
                                                    for: indexPath) as! StickerPackCell
      let stickerPack      = self.stickerPacks[indexPath.row]
      cell.imageView.image = stickerPack.icon
      cell.nameLabel.text  = stickerPack.name
      cell.transform       = CGAffineTransform.identity
      return cell
    case .texts:
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "text",
                                                    for: indexPath) as! TextCell

      let fontName = self.fontNames[indexPath.row]

      cell.label.font = UIFont(name: fontName,
                               size: cell.label.font.pointSize)
      cell.transform = CGAffineTransform.identity
      return cell
    }
  }

  func collectionView(_ collectionView: UICollectionView,
                      willDisplay cell: UICollectionViewCell,
                      forItemAt indexPath: IndexPath) {
    if collectionView != self.collectionViewWrapper.subCollectionView {
      return
    }

    UIView.animate(withDuration: 0.3, animations: {
      cell.transform = CGAffineTransform(scaleX: 1.025, y: 1.025)
    }, completion: { _ in
      UIView.animate(withDuration: 0.3) {
        cell.transform = CGAffineTransform.identity
      }
    })
  }

  func collectionView(_ collectionView: UICollectionView,
                      didEndDisplaying cell: UICollectionViewCell,
                      forItemAt indexPath: IndexPath) {
    if collectionView != self.collectionViewWrapper.subCollectionView {
      return
    }

    cell.transform = CGAffineTransform(scaleX: 0, y: 0)
  }

  func collectionView(_ collectionView: UICollectionView,
                      shouldSelectItemAt indexPath: IndexPath) -> Bool {
    return true
  }

  func collectionView(_ collectionView: UICollectionView,
                      didSelectItemAt indexPath: IndexPath) {
    if collectionView == self.collectionViewWrapper.subCollectionView {
      let sticker = self.selectedPack!.stickers[indexPath.item]

      guard let image = UIImage(named: sticker.img) else { return }

      let w = min(max(image.size.width, image.size.height),
                  self.editorView.frame.width * 0.5)

      let c = CGPoint(x: (self.editorView.frame.width  - w) * 0.5,
                      y: (self.editorView.frame.height - w) * 0.5)

      let frm            = CGRect(origin: c, size: CGSize(width: w, height: w))
      let editView       = EditView(frm, image)
      editView.delegate  = self
      editView.alpha     = 0
      editView.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
      self.editorView.addSubview(editView)

      UIView.animate(withDuration: 0.2, animations: {
        editView.alpha     = 1.0
        editView.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
      }, completion: { _ in
        UIView.animate(withDuration: 0.2, animations: {
          editView.transform = CGAffineTransform.identity
        })
      })

      Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
        AnalyticsParameterItemID: "id-sticker" as NSObject,
        AnalyticsParameterItemName: "Pack: \(self.selectedPack!.name) - Sticker: \(sticker.img)" as NSObject,
        AnalyticsParameterContentType: "cont" as NSObject
      ])

      return
    }

    switch self.section {
    case .filters:
      let filterName = "\(self.filterNames[indexPath.row])"
      filter = CIFilter(name: filterName)
      filter.setValue(CIImage(image: self.image), forKey: kCIInputImageKey)

      let outputImage = filter.outputImage
      let cgImage     = context.createCGImage(outputImage!, from: outputImage!.extent)
      let image       = UIImage(cgImage: cgImage!)

      let fadeAnim       = CABasicAnimation(keyPath: "contents");
      fadeAnim.fromValue = self.imageView.image!
      fadeAnim.toValue   = image
      fadeAnim.duration  = 0.5

      self.imageView.layer.add(fadeAnim, forKey: "contents")
      self.imageView.image = image

      Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
        AnalyticsParameterItemID: "id-filter" as NSObject,
        AnalyticsParameterItemName: "Filter: \(filterName)" as NSObject,
        AnalyticsParameterContentType: "cont" as NSObject
      ])
    case .stickerPacks:
      let cell = collectionView.cellForItem(at: indexPath)!
      self.selectedPack = self.stickerPacks[indexPath.row]

      if self.collectionViewWrapper.subCollectionView.alpha == 0.0 {
        UIView.animate(withDuration: 0.3, animations: {
          for c in collectionView.visibleCells {
            if c != cell {
              c.alpha = 0.0
            }
          }

          cell.transform = CGAffineTransform(translationX: -cell.frame.minX, y: 0)
        }, completion: { _ in })

        self.collectionViewWrapper.collectionView.isScrollEnabled    = false
        self.collectionViewWrapper.subCollectionView.isScrollEnabled = true
        self.collectionViewWrapper.subCollectionView.alpha           = 1.0

        for c in self.collectionViewWrapper.subCollectionView.visibleCells {
          c.transform = CGAffineTransform(scaleX: 0, y: 0)
        }
        self.collectionViewWrapper.subCollectionView.reloadData()
      } else {
        UIView.animate(withDuration: 0.1, animations: {
          for c in self.collectionViewWrapper.subCollectionView.visibleCells {
            c.transform = CGAffineTransform(scaleX: 1.025, y: 1.025)
          }
        }, completion: { _ in
          UIView.animate(withDuration: 0.3, animations: {
            for c in self.collectionViewWrapper.subCollectionView.visibleCells {
              c.transform = CGAffineTransform(scaleX: 0.0001, y: 0.0001)
            }
          }, completion: { _ in
            self.collectionViewWrapper.collectionView.isScrollEnabled    = true
            self.collectionViewWrapper.subCollectionView.isScrollEnabled = false
            self.collectionViewWrapper.subCollectionView.alpha           = 0.0

            UIView.animate(withDuration: 0.3, animations: {
              for c in collectionView.visibleCells {
                if c != cell {
                  c.alpha = 1.0
                }
              }
              cell.transform = CGAffineTransform(translationX: -cell.frame.minX, y: 0)
            })
          })
        })
      }

      Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
        AnalyticsParameterItemID: "id-stickerpack" as NSObject,
        AnalyticsParameterItemName: "Sticker Pack: \(self.selectedPack!.name)" as NSObject,
        AnalyticsParameterContentType: "cont" as NSObject
      ])
      break
    case .texts:
      let cell = collectionView.cellForItem(at: indexPath)! as! TextCell

      let w = CGFloat(200.0)
      let h = CGFloat(50.0)
      let c = CGPoint(x: (self.editorView.frame.width  - w) * 0.5,
                      y: (self.editorView.frame.height - h) * 0.5)

      let frm            = CGRect(origin: c, size: CGSize(width: w, height: h))
      let editView       = EditView(frame: frm)
      editView.delegate  = self
      editView.alpha     = 0
      editView.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
      self.editorView.addSubview(editView)

      editView.font = cell.label.font

      editView.beginTyping()
      UIView.animate(withDuration: 0.2, animations: {
        editView.alpha     = 1.0
        editView.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
      }, completion: { _ in
        UIView.animate(withDuration: 0.2, animations: {
          editView.transform = CGAffineTransform.identity
        })
      })

      Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
        AnalyticsParameterItemID: "id-text" as NSObject,
        AnalyticsParameterItemName: "Text: \(cell.label.font.fontName)" as NSObject,
        AnalyticsParameterContentType: "cont" as NSObject
      ])
    }
  }
}

extension MainViewController: EditViewDelegate {
  func editView(tapped editView: EditView) {
    UIView.animate(withDuration: 0.3) {
      self.colorPicker.selectedColorView.center = editView.colorPoint
    }
  }

  func editView(dblTapped editView: EditView) {
    UIView.animate(withDuration: 0.3, animations: {
      self.colorPicker.selectedColorView.center = editView.colorPoint
      self.colorPickerContainerView.transform   = CGAffineTransform(scaleX: 1, y: 1.05)
    }) { _ in
      UIView.animate(withDuration: 0.3, animations: {
        self.colorPicker.selectedColorView.center = editView.colorPoint
        self.colorPickerContainerView.transform   = .identity
      })
    }
  }

  func touchBegan(_ editView: EditView) {
    self.selectedItem = editView
    editView.superview?.bringSubview(toFront: editView)
  }

  func touchEnded(_ editView: EditView) {
    if !self.deleteButton.transform.isIdentity {
      editView.isUserInteractionEnabled = false
      let center    = self.deleteButton.center
      let transform = editView.transform.scaledBy(x: 0.0001, y: 0.0001)
      UIView.animate(withDuration: 0.3, animations: {
        self.deleteButton.transform = .identity
        editView.center    = center
        editView.transform = transform
      }, completion: { _ in
        editView.removeAction(self)
      })
    }

    UIView.animate(withDuration: 0.3, animations: {
      self.deleteButton.alpha = 0.0
    })

//    self.selectedItem = nil
  }

  func didMove(_ editView: EditView, point: CGPoint) {
    if self.deleteButton.alpha == 0 {
      UIView.animate(withDuration: 0.2, animations: {
        self.deleteButton.alpha = 1.0
      })
    }

//    if editView.frame.contains(self.deleteButton.frame) {
    let delFrame = self.deleteButton.frame.insetBy(dx: -50, dy: -50)
    if delFrame.contains(point) {
      UIView.animate(withDuration: 0.3, animations: {
        self.deleteButton.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        editView.alpha = 0.5
      })
    } else {
      UIView.animate(withDuration: 0.3, animations: {
        self.deleteButton.transform = .identity
        editView.alpha = 1.0
      })
    }
  }
}

extension MainViewController: ColorPickerViewDelegate {
  func colorPickerView(didSelect picker: ColorPickerView,
                       color: UIColor,
                       point: CGPoint) {
    self.selectedItem?.applyColor(color)
    self.selectedItem?.colorPoint = point
  }
}

extension MainViewController : ResizeViewControllerDelegate {
  func resizeViewController(_ controller: ResizeViewController,
                            didFinishCroppingImage image: UIImage,
                            transform: CGAffineTransform,
                            cropRect: CGRect) {

  }

  func resizeViewControllerDidCancel(_ controller: ResizeViewController) {
    controller.dismiss(animated: true, completion: nil)
  }

  func resizeViewController(_ controller: ResizeViewController,
                            didFinishCroppingImage image: UIImage) {
    controller.dismiss(animated: true, completion: nil)
    self.imageView.image = image
    self.image = image
  }
}

